abstract type Integrator end

struct VelocityVerlet <: Integrator
end

function step!() where {S<:State}
end

function evolve!() where {I<:Integrator,S<:State}
end
