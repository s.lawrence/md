#!/usr/bin/env julia

using ArgParse

function simulate()
    # Parameters

    N = 100
    L = [2., 2., 2.]
    D = length(L)
    M = 1.
    Temperature = 1.
    Time = 10.
    Dt = 0.01

    # Potential

    # Initialize

    x = rand(Float64, (D,N)).*L
    p = zeros(N,D+1)

    # Evolve

    for t = 1:Dt:Time
    end
end

function main()
    s = ArgParseSettings()
    @add_arg_table s begin
    end
    args = parse_args(ARGS, s)
end

main()

