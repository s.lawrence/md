abstract type Space end

mutable struct NaiveLookup <: Space
    N::Int
end

function NaiveLookup()::NaiveLookup
    NaiveLookup(0)
end

function empty!(s::NaiveLookup)
    s.N = 0
end

function place!(s::NaiveLookup, x::Vector{Float64}, i::Int)
    if i > s.N
        s.N = i
    end
end

struct NaiveNeighbors
    s::NaiveLookup
end

function neighbors(s::NaiveLookup, r::Float64)::NaiveNeighbors
    NaiveNeighbors(s)
end

struct BinGrid <: Space
end

function empty!(s::BinGrid)
end

function place!(s::BinGrid, x::Vector{Float64}, i::Int)
end

struct BinNeighbors
end

struct FineGrid <: Space
end

function empty!(s::FineGrid)
end

function place!(s::FineGrid, x::Vector{Float64}, i::Int)
end

struct FineNeighbors
end

abstract type State end

struct Particles{S<:Space} <: State
    time::Float64
    space::S
    x::Matrix{Float64}
    v::Matrix{Float64}
end

function Particles(S::Type{<:Space}, x::Matrix{Float64}, v::Matrix{Float64})::Particles{S}
    time::Float64 = 0.
    space = S()
    return Particles(time, space, x, v)
end

