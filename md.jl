#!/usr/bin/env julia

"""
TODO

Immediate:

- Implement grid-based algorithms

Longer term:

- Argument parsing
- Multiple species (polydispersity)
- Inhomogeneous potentials
- Internal (continuous) degrees of freedom
- Wall boundary conditions
"""

using ArgParse
using BenchmarkTools
using Random: MersenneTwister, rand!, randn!
using Statistics: mean

include("state.jl")
include("integrator.jl")
include("potential.jl")

@kwdef struct Observations
    time::Array{Float64}
    potential::Array{Float64}
    kinetic::Array{Float64}
    shear_amplitude::Array{Float64}
end

@kwdef struct Simulation
    D::Int
    L::Float64
    N::Int
    T::Float64
    DT::Float64
    V0::Float64
end

function observe!(obs::Observations, sim::Simulation)
end

function simulate(sim::Simulation)
    # Initialize
    x = zeros((sim.N, sim.D))
    v = zeros((sim.N, sim.D))
    state::Particles{NaiveLookup} = Particles(NaiveLookup, x, v)
end

function main()
    s = ArgParseSettings()
    @add_arg_table s begin
    end
    args = parse_args(ARGS, s)

    sim = Simulation(D = 2,
                     L = 10.,
                     N = 100,
                     T = 10.,
                     DT = 1e-2,
                     V0 = 1.
                    )
    simulate(sim)
end

main()

