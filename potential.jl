using LinearAlgebra: norm

abstract type Potential end

struct Yukawa <: Potential
    R::Float64
    g²::Float64
end

function (yukawa::Yukawa)(x::Vector{Float64})::Float64
    r = norm(x)
    d = length(x)
    if d == 2
        poly = log(r)
    else
        poly = r^(2-d)
    end
    return yukawa.g² * exp(-r / yukawa.R) * poly
end

function force!(f::Vector{Float64}, yukawa::Yukawa, x::Vector{Float64})
    r = norm(x)
    d = length(x)
    mag = yukawa.g² * exp(-r / yukawa.R) * (0.) # TODO
    f = mag * x
end

struct Truncated{V<:Potential} <: Potential
    raw::V
    range::Float64
    shift::Float64
end

function Truncated(raw::V, range::Float64) where {V<:Potential}
    # TODO
    return Truncated(raw, range, shift)
end

function (tr::Truncated{V})(x::Vector{Float64})::Float64 where {V<:Potential}
    # TODO
end

function force!(f::Vector{Float64}, tr::Truncated{V}, x::Vector{Float64}) where {V<:Potential}
    # TODO
end

