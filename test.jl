#!/usr/bin/env julia

using Test

include("state.jl")
include("integrator.jl")
include("potential.jl")

function test_empty(s::S) where {S<:Space}
end

function test_space(S::Type{<:Space})
    s = S()
    test_empty(s)
    empty!(s)
    test_empty(s)
    empty!(s)
end

@testset "Yukawa force" begin
end

@testset "Truncated Yukawa force" begin
end

@testset "Naive lookup" test_space(NaiveLookup)
@testset "Bin grid" test_space(BinGrid)
@testset "Fine grid" test_space(FineGrid)

